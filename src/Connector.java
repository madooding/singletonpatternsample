
public class Connector {
	private static Connector instance;
	private int randomNum;
	private Connector(){
		randomNum = (int)(Math.random() * 254) + 1;
	}
	
	public static Connector getInstance(){
		if(instance == null){
			instance = new Connector();
		}
		return instance;
	}
	
	public void connect(){
		System.out.println("Connecting to 192.168.1." + randomNum + " ...");
	}
}
